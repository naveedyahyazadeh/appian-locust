################
Limitations
################

Currently *unsupported* Appian Interactions
********************************************

* Clicking links with the same name after the first
* Chained forms
* Legacy forms

Limitations when running Locust
**********************************

* On Windows, running locust in distributed mode is not supported
